<?php
function unversity_post_types()
{
	// Events Post Type
    register_post_type('event', array(
    	// create new unique capability 
    	'capability_type' => 'event',// par default equal 'post'
    	'map_meta_cap' => true,// force and require this option 'capability'
    	'supports' => array('title','editor','excerpt'),
    	'rewrite' => array('slug' => 'events'),// this is for slug of link default slu will be event for make him events use this 
    	'has_archive' => true, // for use the archive page 
    	'public' => true,
    	'labels' => array(
	      	'name' => 'Events',
	     	'add_new_item' => 'Add New Event', // for dashboaed panel in wordpress 
	      	'edit_item' => 'Edit Event',
	      	'all_items' => 'All Events',
	      	'singular_name' => 'Event'
    	),
    	'menu_icon' => 'dashicons-calendar' // icon in menu 
  	));

  	// Programs Posts type 
  	register_post_type('program', array(
    	'supports' => array('title'),
    	'rewrite' => array('slug' => 'programs'),// this is for slug of link default slu will be event for make him events use this 
    	'has_archive' => true, // for use the archive page 
    	'public' => true,
    	'labels' => array(
	      	'name' => 'programs',
	     	'add_new_item' => 'Add New Program', // for dashboaed panel in wordpress 
	      	'edit_item' => 'Edit Program',
	      	'all_items' => 'All Programs',
	      	'singular_name' => 'Program'
    	),
    	'menu_icon' => 'dashicons-awards' // icon in menu 
  	)); 
  	// Professor Post Type
  	register_post_type('Professor', array(
  		'show_in_rest' => true,
    	'supports' => array('title','editor','thumbnail'),
    	'public' => true,
    	'labels' => array(
	      	'name' => 'professors',
	     	'add_new_item' => 'Add New Professor', // for dashboaed panel in wordpress 
	      	'edit_item' => 'Edit Professor',
	      	'all_items' => 'All Professors',
	      	'singular_name' => 'Professor'
    	),
    	'menu_icon' => 'dashicons-welcome-learn-more' // icon in menu 
  	));

  	// Campus Post Type 
    register_post_type('campus', array(
    	// create new unique capability 
    	'capability_type' => 'campus',// par default equal 'post'
    	'map_meta_cap' => true,// force and require this option 'capability'
    	'supports' => array('title','editor'),
    	'rewrite' => array('slug' => 'campuses'),// this is for slug of link default slu will be event for make him events use this 
    	'has_archive' => true, // for use the archive page 
    	'public' => true,
    	'labels' => array(
	      	'name' => 'Campuses',
	     	'add_new_item' => 'Add New Campus', // for dashboaed panel in wordpress 
	      	'edit_item' => 'Edit Campus',
	      	'all_items' => 'All Campuses',
	      	'singular_name' => 'Campus'
    	),
    	'menu_icon' => 'dashicons-location-alt' // icon in menu 
  	));

  	 // Note Post Type
	  register_post_type('note', array(
	  	// show in rest for using APP REST json
	  	'capability_type' => 'note',
	  	'map_meta_cap' => true,
	    'show_in_rest' => true,
	    'supports' => array('title', 'editor'),
	    'public' => false,
	    'show_ui' => true,
	    'labels' => array(
	      'name' => 'Notes',
	      'add_new_item' => 'Add New Note',
	      'edit_item' => 'Edit Note',
	      'all_items' => 'All Notes',
	      'singular_name' => 'Note'
	    ),
	    'menu_icon' => 'dashicons-welcome-write-blog'
	  ));

	 
	  // Like Post Type
	  register_post_type('like', array(
	  	// show in rest for using APP REST json
	  	
	    'supports' => array('title'),
	    'public' => false,
	    'show_ui' => true,
	    'labels' => array(
	      'name' => 'Likes',
	      'add_new_item' => 'Add New Like',
	      'edit_item' => 'Edit Like',
	      'all_items' => 'All Likes',
	      'singular_name' => 'Like'
	    ),
	    'menu_icon' => 'dashicons-heart'
	  ));
 }

add_action('init', 'unversity_post_types') ; 


?>