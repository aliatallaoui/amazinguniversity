<?php get_header(); ?>

<?php while (have_posts()) {
    the_post();
    pageBanner(); ?>


<div class="container container--narrow page-section">
  <div class="metabox metabox--position-up metabox--with-home-link">
    <p><a class="metabox__blog-home-link" href="<?php echo get_post_type_archive_link('program') ?>"><i class="fa fa-home" aria-hidden="true"></i>All Programs</a> <span class="metabox__main"><?php the_title(); ?>
    </span></p>
  </div>
  <div class="generic-content">
    <?php the_field('main_body_content'); ?>
  </div>
  
  <?php
    $today = date('Ymd');
    $argsProfessor = array(
       'posts_per_page' => -1,
       'post_type' => 'professor',
       'post_status' => 'publish',
       'orderby' => 'title',
       'order' => 'ASC',
       'meta_query' => array(
         array(
           'key' => 'related_programs',
           'compare' => 'LIKE',
           'value' => '"'.get_the_ID().'"'
           
         )
       )
      
      );
    

    $homePageProfessor = new WP_Query($argsProfessor);
    
    if ($homePageProfessor->have_posts()) {
        echo '<hr class="section-break"><h2 class="headline headline--small"> '.get_the_title().' Professors</h2>';
        echo '<ul class="professor-cards" >';
        while ($homePageProfessor->have_posts()) {
            $homePageProfessor->the_post(); ?>
          
            <li class="professor-card__list-item">
              <a class="professor-card" href="<?php the_permalink() ?>">
                <img class="professor-card__image" src="<?php the_post_thumbnail_url('professorLandscape'); ?>">
                <span class="professor-card__name"><?php the_title() ?></span>
              </a>
            </li>

          <?php
        }
        echo '</ul>';
    }
    wp_reset_postdata(); ?>
  <hr class="section-break">
  
  <!-- The For Events Related with This Program -->
  <?php
    $today = date('Ymd');
    $args = array(
       'posts_per_page' => -1,
       'post_type' => 'Event',
       'post_status' => 'publish',
       'meta_key' => 'event_date',
       'orderby' => 'meta_value_num',
       'order' => 'ASC',
       'meta_query' => array(
         array(
           'key' => 'event_date',
           'compare' => '>=',
           'value' => $today,
           'type' => 'numeric'
         ),
         array(
           'key' => 'related_programs',
           'compare' => 'LIKE',
           'value' => '"'.get_the_ID().'"'
           
         )
       )
      
      );
    echo '<h2 class="headline headline--small">Upcoming '.get_the_title().' Events</h2>';
    $homePageEvents = new WP_Query($args);
    
    if ($homePageEvents->have_posts()) {
        while ($homePageEvents->have_posts()) {
            $homePageEvents->the_post(); ?>
          <div class="event-summary">
            <a class="event-summary__date t-center" href="<?php the_permalink(); ?>">
              <?php
              $event_date = get_field('event_date', false, false);
            $event_date = new DateTime($event_date); ?>
              <span class="event-summary__month"><?php echo $event_date->format('M')?></span>
              <span class="event-summary__day"><?php echo $event_date->format('d')?></span>
            </a>
            <div class="event-summary__content">
              <h5 class="event-summary__title headline headline--tiny"><a href="<?php the_permalink(); ?>"><?php the_title() ?></a></h5>
              <p><?php
                    if (has_excerpt()) {
                        echo get_the_excerpt();
                    } else {
                        echo wp_trim_words(get_the_content(), 18);
                    } ?> <a href="<?php the_permalink(); ?>" class="nu gray">Learn more</a></p>
            </div>
          </div>

          <?php
           wp_reset_postdata();

            $relatedCampuses = get_field('related_campus');
            if ($relatedCampuses) {
                echo '<hr class="section-break">';
                echo '<h2 class="headline headline--medium">'.get_the_title().' is Available At These Campuses:</h2>';
            
                echo '<ul class="min-list link-list">';
                foreach ($relatedCampuses as $campus) {
                    ?>
              <li><a href="<?php echo get_the_permalink($campus)  ?>"><?php echo get_the_title($campus); ?></a></li>
              <?php
                }
                echo '</ul>';
            }
        }
    } ?>


  
    

</div>

<?php
}
  get_footer(); ?>
