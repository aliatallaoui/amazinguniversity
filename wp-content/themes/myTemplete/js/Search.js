import $ from 'jquery';

class Search {
    // 1. describe and create/initiate our object
    constructor() {
        this.addSearchHTML();
        this.previousValue;
        this.resultsDiv = $("#search-overlay__results");
        this.openButton = $(".js-search-trigger");
        this.closeButton = $(".search-overlay__close");
        this.searchOverlay = $(".search-overlay");
        this.serachField = $("#search-term");
        this.events();
        this.isOverlayOpen = false;
        this.typingTimer;
        this.isSpinnerVisible = false;
    }

    // 2. events 
    events() {
        this.openButton.on("click", this.openOverlay.bind(this));
        this.closeButton.on("click", this.closeOverlay.bind(this));
        $(document).on("keydown", this.KeyPressDispatcher.bind(this));
        this.serachField.on("keyup", this.typingLogic.bind(this));
    }


    // 3. methods(function , action ...)
    typingLogic() {
        if (this.serachField.val() != this.previousValue) {

            clearTimeout(this.typingTimer);
            if (this.serachField.val()) {
                if (!this.isSpinnerVisible) {
                    this.resultsDiv.html('<div class="spinner-loader"></div>');
                    this.isSpinnerVisible = true;
                }

                this.typingTimer = setTimeout(this.getResults.bind(this), 750);
            } else {
                this.resultsDiv.html('');
                this.isSpinnerVisible = false;
            }

        }
        this.previousValue = this.serachField.val();
    }
    //getResults it's function run on realtime we use ES6 arrow function because we want acces into resultDiv and we use function map remplace foreach and we using templete using speacial `` cama almohim raha fl good 
    getResults() {
        $.getJSON(universityData.root_url + '/wp-json/university/v1/search?term=' + this.serachField.val(), (results) => {
            this.resultsDiv.html(`
                <div class="row">
                    <div class="one-third">

                        <h2 class="search-overlay__section-title">General Information</h2>
                        ${results.generalInfo.length ? '<ul class="link-list min-list">' : '<p>No General information matches thar search.</p>'}
                        ${results.generalInfo.map(item => `<li><a href="${item.permalink}" >${item.title}</a> ${item.postType == 'post' ? `By 
                        ${item.AuthorName}` : ''}  </li>`).join('')}
                        ${results.generalInfo.length ? '</ul>' : ''}

                    </div>
                    <div class="one-third">

                        <h2 class="search-overlay__section-title">Programs</h2>
                        ${results.programs.length ? '<ul class="link-list min-list">' : `<p>No Programs matches that search.<a href="${universityData.root_url}/programs"> View All Programs</a></p>`}
                        ${results.programs.map(item => `<li><a href="${item.permalink}" >${item.title}</a>  </li>`).join('')}
                        ${results.programs.length ? '</ul>' : ''}


                        <h2 class="search-overlay__section-title">Professors</h2>
                        ${results.professors.length ? '<ul class="professor-cards">' : `<p>No professors matches that search.<a href="${universityData.root_url}/professors"> View All professors</a></p>`}
                        ${results.professors.map(item => `
                        <li class="professor-card__list-item">
                            <a class="professor-card" href="${item.permalink}">
                                <img class="professor-card__image" src="${item.photo}">
                                <span class="professor-card__name">${item.title}</span>
                            </a>
                        </li>`).join('')}
                        ${results.professors.length ? '</ul>' : ''}
                    

                    </div>
                    <div class="one-third">

                        <h2 class="search-overlay__section-title">Campsues</h2>
                        ${results.campuses.length ? '<ul class="link-list min-list">' : `<p>No Campuses matches that search.<a href="${universityData.root_url}/campuses"> View All Campuses</a></p>`}
                        ${results.campuses.map(item => `<li><a href="${item.permalink}" >${item.title}</a>  </li>`).join('')}
                        ${results.campuses.length ? '</ul>' : ''}

                        <h2 class="search-overlay__section-title">Events</h2>
                        ${results.events.length ? '' : `<p>No events matches that search.<a href="${universityData.root_url}/events"> View All events</a></p>`}
                        ${results.events.map(item => `

<div class="event-summary">
  <a class="event-summary__date t-center" href="#">
    <span class="event-summary__month">${item.month}</span>
    <span class="event-summary__day">${item.day}</span>  
  </a>
  <div class="event-summary__content">
    <h5 class="event-summary__title headline headline--tiny"><a href="${item.permalink}">${item.title}</a></h5>
    <p>${item.excerpt} <a href="${item.permalink}" class="nu gray">Learn more</a></p>
  </div>
</div>
                        
                        `).join('')}
                        



                        
            `);
            this.isSpinnerVisible = false;
        });


    }

    openOverlay() {
        this.searchOverlay.addClass("search-overlay--active");
        $("body").addClass("body-no-scroll");
        this.serachField.val('');
        setTimeout(() => this.serachField.focus(), 400);
        this.isOverlayOpen = true;
        console.log('the function OpenOverlay run!');
        return false;
    }
    closeOverlay() {
        this.searchOverlay.removeClass("search-overlay--active");
        $("body").removeClass("body-no-scroll");
        this.isOverlayOpen = false;
        console.log('the function closeOverlay run !');
    }
    KeyPressDispatcher(e) {


        if (e.keyCode == 83 && !this.isOverlayOpen && !$("input, textarea").is(':focus')) {
            this.openOverlay();
        }
        if (e.keyCode == 27 && this.isOverlayOpen) {
            this.closeOverlay();
        }
    }

    addSearchHTML() {
        $("body").append(`
            <div class="search-overlay">
            <div class="search-overlay__top">
                <div class="container">
                <i class="fa fa-search search-overlay__icon" aria-hidden="true"></i>
                <input type="text" class="search-term" placeholder="What are you looking for ? " id="search-term">
                <i class="fa fa-window-close search-overlay__close" aria-hidden="true"></i>
                </div>
            </div>
            <div class="container">
                <div id="search-overlay__results">
                </div>
            </div>
            </div>
        `);
    }


}

export default Search;