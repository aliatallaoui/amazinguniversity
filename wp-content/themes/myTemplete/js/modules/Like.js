import $ from 'jquery';


class Like {
    constructor() {

        this.events();
    }
    //events 
    events() {
        $(".like-box").on("click", this.ourClickDispatcher.bind(this))
    }

    // methods

    ourClickDispatcher(e) {
        var currentLikeBox = $(e.target).closest(".like-box");
        if (currentLikeBox.attr('data-exists') == 'yes') {
            this.deleteLike(currentLikeBox);
        } else {
            this.createLike(currentLikeBox);
        }
    }

    createLike(currentLikeBox) {

        $.ajax({
            beforeSend: (xhr) => {
                xhr.setRequestHeader('X-WP-Nonce', universityData.nonce);
            },
            url: universityData.root_url + '/wp-json/university/v1/manageLike',
            type: 'POST',
            data: {
                'professorId': currentLikeBox.data('professor')
            },
            success: (response) => {
                currentLikeBox.attr('data-exists', 'yes');
                // 10: covert decemial 
                var likeCount = parseInt(currentLikeBox.find(".like-count").html(), 10);
                likeCount++;
                currentLikeBox.find(".like-count").html(likeCount);
                // update data-like we use response because it's return id number of post created 
                currentLikeBox.attr("data-like", response);
                console.log("Great create Like");
                console.log(response);
            },
            error: (response) => {
                console.log("Sorry create Like");
                console.log(response);
            }
        });
    }

    deleteLike(currentLikeBox) {
        $.ajax({
            beforeSend: (xhr) => {
                xhr.setRequestHeader('X-WP-Nonce', universityData.nonce);
            },
            url: universityData.root_url + '/wp-json/university/v1/manageLike',
            data: {
                'like': currentLikeBox.attr('data-like')
            },
            type: 'DELETE',
            success: (response) => {
                currentLikeBox.attr('data-exists', 'no');
                // 10: covert decemial 
                var likeCount = parseInt(currentLikeBox.find(".like-count").html(), 10);
                likeCount--;
                currentLikeBox.find(".like-count").html(likeCount);
                // update data-like we use response because it's return id number of post created 
                currentLikeBox.attr("data-like", '');
                console.log("Great delete like");
                console.log(response);
            },
            error: (response) => {
                console.log("Sorry delete like");
                console.log(response);
            }
        });
    }


}

export default Like;