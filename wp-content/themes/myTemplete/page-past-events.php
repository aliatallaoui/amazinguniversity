<?php
  get_header();
  pageBanner(array(
    'title' => 'Past Events',
    'subtitle' => 'A racap of out past events'

  ));

?>

<div class="container container--narrow page-section">
  <?php

    $today = date('Ymd');
    $args = array(
        'paged' => get_query_var('paged', 1),// this for pagination custom query we put 1 for default value if not existe
        'posts_per_page' =>5,
        'post_type' => 'Event',
        'meta_key' => 'event_date',
        'orderby' => 'meta_value_num',
        'order' => 'ASC',
        'meta_query' => array(
            array(
            'key' => 'event_date',
            'compare' => '<',
            'value' => $today,
            'type' => 'numeric'
            )
        )
    
    );
    $pastpageEvents = new WP_Query($args);
    if ($pastpageEvents->have_posts()) {
        while ($pastpageEvents->have_posts()) {
            $pastpageEvents->the_post();
            $event_date = get_field('event_date', false, false);
            $event_date = new DateTime($event_date); ?>
    
          <div class="event-summary">
                <a class="event-summary__date t-center" href="<?php the_permalink(); ?>">
                  <span class="event-summary__month"><?php echo $event_date->format('M')?></span>
                  <span class="event-summary__day"><?php echo $event_date->format('d')?></span>
                </a>
                <div class="event-summary__content">
                  <h5 class="event-summary__title headline headline--tiny"><a href="<?php the_permalink(); ?>"><?php the_title() ?></a></h5>
                  <p><?php echo wp_trim_words(get_the_content(), 20) ?> <a href="<?php the_permalink(); ?>" class="nu gray">Learn more</a></p>
                </div>
              </div>
      <?php
        }
    }
    echo paginate_links(array(
        'total' => $pastpageEvents->max_num_pages
    )); ?>
</div>

<?php
  get_footer();
?>


