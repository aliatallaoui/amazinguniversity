<div class="generic-content">
    <form class="search-form"  action="<?php echo esc_url(site_url('/')) ?>" method="get">
        <label class="headline headline--medium" for="s">Perform a New Search:</label>
        <div class="search-form-row">

            <input class="s" type="search" placeholder="What are you looking for !"  name="s" id="s">
            <input class="search-submit" type="submit" value="Search">
        </div>
    </form>
  </div>