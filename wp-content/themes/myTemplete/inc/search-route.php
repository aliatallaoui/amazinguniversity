<?php

function universitySearchResutls($data)
{
    $mainQuery = new WP_Query(array(
        'post_type'=> array('professor','page','post','program','campus','event'),
        's' => sanitize_text_field($data['term'])
    ));



    $Results = array(
        'generalInfo' => array(),
        'professors' => array(),
        'events' => array(),
        'campuses' => array(),
        'programs' => array()
    );
    
    while ($mainQuery->have_posts()) {
        $mainQuery->the_post();

        if (get_post_type() == 'post' or get_post_type() == 'page') {
            array_push($Results['generalInfo'], array(
                'title'=>  get_the_title(),
                'permalink' => get_the_permalink(),
                'postType' => get_post_type(),
                'AuthorName' => get_the_author()
            ));
        }
        if (get_post_type() == 'professor') {
            array_push($Results['professors'], array(
                'title'=>  get_the_title(),
                'permalink' => get_the_permalink(),
                'photo' => get_the_post_thumbnail_url(get_the_ID(), 'professorLandscape')

            ));
        }
        if (get_post_type() == 'program') {
            $relatedCompus = get_field('related_campus', get_the_id());
            if ($relatedCompus) {
                foreach ($relatedCompus as $campus) {
                    array_push($Results['campuses'], array(
                        'title'=>  get_the_title($campus),
                        'permalink' => get_the_permalink($campus)
                    ));
                }
            }

            array_push($Results['programs'], array(
                'title'=>  get_the_title(),
                'permalink' => get_the_permalink(),
                'id' => get_the_ID()
                
            ));
        }
        if (get_post_type() == 'event') {
            $eventDate = new DateTime(get_field('event_date', false, false));
            $excerpt = null;
            if (has_excerpt()) {
                $excerpt =  get_the_excerpt();
            } else {
                $excerpt =  wp_trim_words(get_the_content(), 10);
            }
            array_push($Results['events'], array(
                'title'=>  get_the_title(),
                'permalink' => get_the_permalink(),
                'month' => $eventDate->format('M'),
                'day' => $eventDate->format('d'),
                'excerpt' => $excerpt
            ));
        }

        if (get_post_type() == 'campus') {
            array_push($Results['campuses'], array(
                'title'=>  get_the_title(),
                'permalink' => get_the_permalink(),
            ));
        }
    }
    wp_reset_postdata();

    if ($Results['programs']) {
        $programsMetaQuery = array('relation' => 'OR');

        foreach ($Results['programs'] as $item) {
            array_push($programsMetaQuery, array(
                        'key' => 'related_programs',
                        'compare' => 'LIKE',
                        'value' => '"'.$item['id'].'"',
                    ));
        }

        $programRelationshipQuery = new WP_Query(array(
                'post_type' => array('professor','event'),
                'meta_query' => $programsMetaQuery
                
            ));
            
        if ($programRelationshipQuery->have_posts()) {
            while ($programRelationshipQuery->have_posts()) {
                $programRelationshipQuery->the_post();
                if (get_post_type() == 'professor') {
                    array_push($Results['professors'], array(
                        'title'=>  get_the_title(),
                        'permalink' => get_the_permalink(),
                        'photo' => get_the_post_thumbnail_url(get_the_ID(), 'professorLandscape')
                    ));
                }
                if (get_post_type() == 'event') {
                    $eventDate = new DateTime(get_field('event_date', false, false));
                    $excerpt = null;
                    if (has_excerpt()) {
                        $excerpt =  get_the_excerpt();
                    } else {
                        $excerpt =  wp_trim_words(get_the_content(), 10);
                    }
                    array_push(
                        $Results['events'],
                        array(
                            'title'=>  get_the_title(),
                            'permalink' => get_the_permalink(),
                            'month' => $eventDate->format('M'),
                            'day' => $eventDate->format('d'),
                            'excerpt' => $excerpt
                        )
                    );
                }
            }
        }

        $Results['professors'] = array_values(array_unique($Results['professors'], SORT_REGULAR));
        $Results['events'] = array_values(array_unique($Results['events'], SORT_REGULAR));
    }
    return $Results;
}

function universityRegisterSearch()
{
    register_rest_route('university/v1', 'search', array(
        'methods' => WP_REST_SERVER::READABLE,
        'callback' => 'universitySearchResutls'
    ));
}
add_action('rest_api_init', 'universityRegisterSearch');
