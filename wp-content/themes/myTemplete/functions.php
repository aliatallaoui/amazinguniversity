<?php
  require get_theme_file_path('/inc/search-route.php');
  require get_theme_file_path('/inc/like-route.php');

  function university_custom_rest()
  {
      // you can create many register_rest_filed for anything for image or information what you need.. return something php(custom filed ,... ) for using in javascript in realtime
      register_rest_field('post', 'authorName', array('get_callback' => function () {
          return get_the_author();
      }));
      register_rest_field('note', 'userNoteCount', array('get_callback' => function () {
          return count_user_posts(get_current_user_id(), 'note');
      }));
  }

  add_action('rest_api_init', 'university_custom_rest');

  function pageBanner($args = null)
  {
      if (!$args['title']) {
          $args['title'] = get_the_title();
      }
      if (!$args['subtitle']) {
          $args['subtitle'] = get_field('page_banner_subtitle');
      }

      if (!$args['photo']) {
          if (get_field('page_banner_background_image')) {
              $args['photo'] = get_field('page_banner_background_image')['sizes']['pageBanner'];
          } else {
              $args['photo'] = get_theme_file_uri('/images/ocean.jpg');
          }
      } ?>
    <div class="page-banner">
  <div class="page-banner__bg-image"
    style="background-image: url(<?php echo $args['photo'] ?>);"></div>
  <div class="page-banner__content container container--narrow">
    <h1 class="page-banner__title"><?php echo $args['title']; ?></h1>
    <div class="page-banner__intro">
      <p><?php echo $args['subtitle']; ?></p>
    </div>
  </div>
</div>

    <?php
  }


  function university_files()
  {
      // in google map you need API_KEY but for now i don't have card for that.
      wp_enqueue_script('googleMap', '//maps.googleapis.com/maps/api/js', null, '1.0', true);
      wp_enqueue_script('main-university1-js', get_theme_file_uri('/js/scripts-bundled.js'), null, '1.0', true);
      wp_enqueue_style('custom-google_font', 'https://fonts.googleapis.com/css?family=Roboto+Condensed:300,300i,400,400i,700,700i|Roboto:100,300,400,400i,700,700i');
      wp_enqueue_style('font-awesome', 'https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css');
      wp_enqueue_style('university_main_styles', get_stylesheet_uri());

      //
      wp_localize_script('main-university1-js', 'universityData', array(
        'root_url' => get_site_url(),
        'nonce' => wp_create_nonce('wp_rest'),
      ));
  }

  add_action('wp_enqueue_scripts', 'university_files');

  function university_features()
  {
      // register_nav_menu('headerMenuLocation','Header Menu Location');
      // register_nav_menu('footerLocationOne','Footer Location One');
      // register_nav_menu('footerLocationTwo','Footer Location Two');
      add_theme_support('title-tag');
      add_theme_support('post-thumbnails');// it's add abillty for upload image in the dashboard
      add_image_size('professorLandscape', 400, 260, true);
      add_image_size('professorPortrait', 480, 650, true);
      add_image_size('pageBanner', 1500, 350, true);
  }
  add_action('after_setup_theme', 'university_features');

// function wpdocs_custom_excerpt_length($length)
// {
//     return 20;
// }
// add_filter('excerpt_length', 'wpdocs_custom_excerpt_length', 100);

function university_adjust_queries($query)
{
    // campus query
    if (!is_admin() and is_post_type_archive('campus') and $query->is_main_query()) {
        $query->set('posts_per_page', -1);
    }

    // programs query
    if (!is_admin() and is_post_type_archive('program') and $query->is_main_query()) {
        $query->set('orderby', 'title');
        $query->set('order', 'ASC');
        $query->set('posts_per_page', -1);
    }

    // this for manipule query of events
    if (!is_admin() and is_post_type_archive('event') and $query->is_main_query()) {
        $today = date('Ymd');
        $query->set('meta_key', 'event_date');
        $query->set('orderby', 'meta_value_num');
        $query->set('order', 'ASC');
        $query->set('meta_query', array(
      array(
        'key' => 'event_date',
        'compare' => '>=',
        'value' => $today,
        'type' => 'numeric'
      )
    ));
    }
}

add_action('pre_get_posts', 'university_adjust_queries');
 
 
 
 
 
// Redirect subcriber account out of admin and onto homepage

add_action('admin_init', 'redirectSubsToFrontend');

function redirectSubsToFrontend()
{
    $ourCurrentUser = _wp_get_current_user();
    if (count($ourCurrentUser->roles) == 1 and $ourCurrentUser->roles[0] == 'subscriber') {
        wp_redirect(site_url('/'));
        exit;
    }
}
// hide admin bar
add_action('wp_loaded', 'noSubsAdminBar');

function noSubsAdminBar()
{
    $ourCurrentUser = _wp_get_current_user();
    if (count($ourCurrentUser->roles) == 1 and $ourCurrentUser->roles[0] == 'subscriber') {
        show_admin_bar(false);
    }
}


// Customize Login Screen logo ta3 WordPress
add_filter('login_headerurl', 'ourHeaderUrl');

function ourHeaderUrl()
{
    return esc_url(site_url('/'));
}


add_action('login_enqueue_scripts', 'ourLoginCSS');

function ourLoginCSS()
{
    wp_enqueue_style('custom-google_font', 'https://fonts.googleapis.com/css?family=Roboto+Condensed:300,300i,400,400i,700,700i|Roboto:100,300,400,400i,700,700i');
    wp_enqueue_style('font-awesome', 'https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css');
    wp_enqueue_style('university_main_styles', get_stylesheet_uri());
}
// customize  title for login
add_filter('login_headertitle', 'ourLoginTitle');

function ourLoginTitle()
{
    return get_bloginfo('name');
}


// force note posts to be private

add_filter('wp_insert_post_data', 'makeNotePrivate', 10, 2);
// 10: priorete function when you have multi function in wp_insert_post_data  mean:: the order
// 2: mean number parametre work with them

function makeNotePrivate($data, $postarr)
{
    // for delete any code meleshze in data
    if ($data['post_type'] == 'note') {
        if (count_user_posts(get_current_user_id(), 'note') > 4 and !$postarr['ID']) {
            die("You have reached your note limit");
        }

        $data['post_title'] = sanitize_text_field($data['post_title']);
        $data['post_content'] = sanitize_textarea_field($data['post_content']);
    }

    if ($data['post_type'] == 'note' and $data['post_status'] != 'trash') {
        $data['post_status'] = "private";
    }

    return $data;
}

// limiting number post
