<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

if (file_exists(dirname(__FILE__) . '/local.php')) {
    define('DB_NAME', 'local');
    define('DB_USER', 'root');
    define('DB_PASSWORD', 'root');
    define('DB_HOST', 'localhost');
} else {
    define('DB_NAME', 'local');
    define('DB_USER', 'root');
    define('DB_PASSWORD', 'root');
    define('DB_HOST', 'localhost');
}

// ** MySQL settings - You can get this info from your web host ** //


/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY', 'l Xk_|CiKS66^lQy4<fi`OD_[Lwe|+ ;nVaBW-KElhri3s4VA6RtN86a60Q<u6Qz');
define('SECURE_AUTH_KEY', 'P(u?DbxuTd]RlumIKhZt`zX>yZW+[BSb]4tJJ0|@8W:pLuGtxg:zKg|h>^:9w7)A');
define('LOGGED_IN_KEY', ']IGs!SN-,k]DLK<_a?g:Nw8T>.>]Y|*4l#%N_obk=b,Avk(2F}$|?Tk<Lr]Xmv_%');
define('NONCE_KEY', '.;K=nwvf8xI&,}eI}x4 $3B@8<w+-ySXG/Xe]Z!hnan0NS|g2uCm!c8w-mKqj%2l');
define('AUTH_SALT', 'gkE|%p0zzQQUJj?4FU9>KR8% =Ed#+=-Ly}EI.`dA(<ilOP:^fK1Vld*:-bn:qvi');
define('SECURE_AUTH_SALT', '{ab MAyZvhwKCy5t7o}lVr}0+zyr d hq<PFrPRKRNu.x.KLB)SFd;V5TWEDi|)_');
define('LOGGED_IN_SALT', 'f_,<z-MIiCpxtP/wI{WXF]BGZ.^xH);oZ;ret;%pjk,/C*(yQKPK.E)jq>+.Su ~');
define('NONCE_SALT', 'T~a]D6RUUK+28(O4+Qev0WQu0_tdR63V42|}<~$#2o|[gYi@7B]p>3!Ayt7+eBWr');


/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';




/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if (! defined('ABSPATH')) {
    define('ABSPATH', dirname(__FILE__) . '/');
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
